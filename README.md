# DoctOpen

## Contexte
Dans le cadre de notre formation chez Simplon, Nous avons travaillé en groupe de 3 personnes sur ce projet. Nous devions créer la partie back-end en prenant aussi en compte la phase de conception et de maquettage. Nous avons créé un diagramme de classe, plusieurs maquettes pour le potentiel rendu de l'application au format bureau et mobile.

## Objectif 
Permettre à un utilisateur de retrouver un spécialiste du milieu médical afin de prendre rendez-vous, et permettre à un professionnel de santé de s’inscrire pour permettre la prise de rendez-vous en ligne via un 
planning configurable.
