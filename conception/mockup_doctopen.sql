-- Insert value role
INSERT INTO
  `role`(`role_name`)
VALUES
  ("ROLE_CUSTOMER"),("ROLE_PROFESSIONAL");
-- Insert value user
INSERT INTO
  `user` (`username`, `password`)
VALUES
  ("Claire", "1234"),("Kevin", "1234"),("Florian", "1234");
-- Insert value between user and role
INSERT INTO
  `user_role`
VALUES
  (1, 1),(2, 2),(3, 2);
-- Insert value customer
INSERT INTO
  `customer`(
    `lastName`,
    `firstName`,
    `birthday`,
    `mail`,
    `id_user`
  )
VALUES
  (
    "Bienvenue",
    "Océane",
    "1975-02-18 00:00:00",
    "oceane.bienvenue@gmail.com",
    3
  );
INSERT INTO
  `customer`(
    `lastName`,
    `firstName`,
    `birthday`,
    `mail`,
    `id_user`
  )
VALUES
  (
    "Larocque",
    "David",
    "1964-08-24 00:00:00",
    "david.larocque@gmail.com",
    2
  );
INSERT INTO
  `customer`(
    `lastName`,
    `firstName`,
    `birthday`,
    `mail`,
    `id_user`
  )
VALUES
  (
    "Garreau",
    "Luc",
    "1984-03-17 00:00:00",
    "luc.garreau@gmail.com",
    1
  );
-- Insert value professional
INSERT INTO
  `professional`(
    `lastName`,
    `firstName`,
    `mail`,
    `address`,
    `phone`,
    `activity_description`,
    `schedule`,
    `id_user`
  )
VALUES
  (
    "Robillard",
    "Elisabeth",
    "elisabeth.robillard@gmail.com",
    "17, rue Reine Elisabeth 77000 Melun",
    "0145448250",
    "C'est bien chez moi viendez !",
    "8h-12h / 12h-17h",
    3
  );
INSERT INTO
  `professional`(
    `lastName`,
    `firstName`,
    `mail`,
    `address`,
    `phone`,
    `activity_description`,
    `schedule`,
    `id_user`
  )
VALUES
  (
    "Peter",
    "Celine",
    "celine.peter@gmail.com",
    "82, rue de Geneve 80000 Amiens",
    "0370237787",
    "Non c'est mieux chez moi viendez !",
    "8h-12h / 12h-17h",
    1
  );
INSERT INTO
  `professional`(
    `lastName`,
    `firstName`,
    `mail`,
    `address`,
    `phone`,
    `activity_description`,
    `schedule`,
    `id_user`
  )
VALUES
  (
    "Benoit",
    "Elodie",
    "elodie.benoit@gmail.com",
    "19, boulevard Amiral Courbet 69000 lyon",
    "0194555131",
    "Oublie les autres, viendez chez moi !",
    "8h-12h / 12h-17h",
    2
  );
-- Insert value city
INSERT INTO
  `city`(`zipcode`, `city_name`)
VALUES
  ("69000", "Lyon"),("80000", "Amiens"),("77000", "Melun"),("75000", "Paris"),("13000", "Marseille");
-- Insert value job
INSERT INTO
  `job`(`job_name`, `speciality`)
VALUES
  ("Médecin généraliste", "général"),("Dentiste", "chirurgien"),("Gynécologue", ""),("Médecin", "Pneumologue"),("Médecin", "Généraliste");
-- Insert value payment
INSERT INTO
  `payment`(`payment_name`)
VALUES
  ("Carte bancaire"),("Espèce"),("Paypal");
-- Insert value availability
INSERT INTO
  `availability`(
    `date`,
    `disponibility`,
    `duration`,
    `start_time_morning`,
    `end_time_morning`,
    `start_time_afternoon`,
    `end_time_afternoon`,
    `id_professional`
  )
VALUES
  (
    "20-10-21 08:00:00",
    1,
    3,
    "20-10-21 08:00:00",
    "20-10-21 12:00:00",
    "20-10-21 13:00:00",
    "20-10-21 17:00:00",
    1
  );
-- Insert value appointment
INSERT INTO
  `appointment`(`date`, `id_customer`, `id_professional`)
VALUES
  ("20-10-21 17:00:00", 1, 1),
  ("20-10-22 17:00:00", 1, 1),
  ("20-10-23 17:00:00", 1, 1);
-- Insert value between professional and city
INSERT INTO
  `professional_city`(`id_professional`, `id_city`)
VALUES
  (1, 3),(1, 2),(1, 1),(1, 2),(1, 1);
-- Insert value between professional and job
INSERT INTO
  `professional_job`(`id_professional`, `id_job`)
VALUES
  (1, 3),(1, 2),(1, 1),(1, 2),(1, 1);
-- Insert value between professional and paiement
INSERT INTO
  `professional_payment`(`id_professional`, `id_payment`)
VALUES
  (1, 3),(1, 2),(1, 1);