package co.simplon.promo16.doctopen.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import co.simplon.promo16.doctopen.dao.CustomerDAO;

@Controller
@RequestMapping("/customers")
public class CustomerController {

    @Autowired
    private CustomerDAO customerDao;

    @GetMapping("/")
    public String getAllCustomer(Model model) {
        model.addAttribute("customers", customerDao.findAll());
        return "customer";
    }

    @GetMapping("/{id}")
    public String getOneCustomer(@PathVariable("id") Integer id, Model model) {
        model.addAttribute("customer", customerDao.findById(id));
        System.out.println(id);
        return "customer";
    }

    // @PostMapping("/")
    // public String createCustome(@RequestBody Customer customer) {
    //     return customerDao.createCustomer(customer);
    // }
}