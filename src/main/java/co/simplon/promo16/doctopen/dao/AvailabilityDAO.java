package co.simplon.promo16.doctopen.dao;

import java.util.List;

import org.springframework.http.ResponseEntity;

import co.simplon.promo16.doctopen.model.Availability;

public interface AvailabilityDAO {
 /**
     * Récupère toutes les disponibilités en base de données et les convertis en
     * entity Availability
     * 
     * @return Une liste d'instance de Availability représentant les disponibilités
     *         contenues en BDD
     */
    public List<Availability> findAll();

    /**
     * Récupère une disponibilité spécifique en se basant sur son id
     * 
     * @param id L'id de la disponibilité à récupérer
     * @return Une instance de Availability ou null si aucune disponibilité n'a cet id
     */
    public Availability findById(Integer id);

    /**
     * Méthode permettant de faire persister une disponibilité en base de données.
     * Son but sera de prendre une instance de Availability et de la convertir en
     * une requête SQL (INSERT INTO)
     * 
     * @param availability L'instance de Availability à faire persister en base de
     *                     données
     */
    public ResponseEntity<String> save(Availability availability);

    /**
     * Méthode qui permet de mettre à jour une disponibilité déjà en base de données
     * (cette méthode n'existera pas vraiment dans les repository Spring
     * car le update est fait aussi à partir du save, mais on la met pour plus
     * de simplicité au début)
     * 
     * @param availability La disponibilité qu'on souhaite mettre à jour en base de
     *                     données
     */
    public ResponseEntity<String> update(Availability availability);

    /**
     * Méthode qui permet de supprimer une disponobilité persistée en base de données
     * en se basant sur son id
     * 
     * @param id l'id de la disponibilité à supprimer
     */
    public ResponseEntity<String> deleteById(Integer id);   
}
