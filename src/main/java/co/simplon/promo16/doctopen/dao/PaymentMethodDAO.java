package co.simplon.promo16.doctopen.dao;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.doctopen.model.Payment;

@Repository
public interface PaymentMethodDAO {
    
    public List<Payment> findAll(); 
    public Payment findById(Integer id);
    public ResponseEntity<String> save(Payment paymentMethod);
    public ResponseEntity<String> update(Payment paymentMethod);
    public ResponseEntity<String> deleteById(Integer id);

}
