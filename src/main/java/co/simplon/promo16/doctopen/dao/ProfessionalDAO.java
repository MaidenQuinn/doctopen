package co.simplon.promo16.doctopen.dao;

import java.util.List;

import org.springframework.http.ResponseEntity;

import co.simplon.promo16.doctopen.model.Professional;

public interface ProfessionalDAO {
    /**
     * Récupère tous les professionnels en base de données et les convertis en
     * entity Professional
     * 
     * @return Une liste d'instance de Professional représentant les professionnels
     *         contenus en BDD
     */
    public List<Professional> findAll();

    /**
     * Récupère un professionnel spécifique en se basant sur son id
     * 
     * @param id L'id du professionnel à récupérer
     * @return Une instance de Professional ou null si aucun professionnel n'a cet
     *         id
     */
    public Professional findById(Integer id);

    /**
     * Méthode permettant de faire persister un professionnel en base de données.
     * Son but sera de prendre une instance de professionnel et de la convertir en
     * une requête SQL (INSERT INTO)
     * 
     * @param professional L'instance de Professional à faire persister en base de
     *                     données
     */
    public ResponseEntity<String> save(Professional professional);

    /**
     * Méthode qui permet de mettre à jour un professionnel déjà en base de données
     * (cette méthode n'existera pas vraiment dans les repository Spring
     * car le update est fait aussi à partir du save, mais on la met pour plus
     * de simplicité au début)
     * 
     * @param professional Le professionnel qu'on souhaite mettre à jour en base de
     *                     données
     */
    public ResponseEntity<String> update(Professional professional);

    /**
     * Méthode qui permet de supprimer un professionnel persisté en base de données
     * en se basant sur son id
     * 
     * @param id l'id du professionnel à supprimer
     */
    public ResponseEntity<String> deleteById(Integer id);

}
