package co.simplon.promo16.doctopen.dao;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.doctopen.model.Role;

@Repository
public interface RoleDAO {
    
    public List<Role> findAll(); 
    public Role findById(Integer id);
    public ResponseEntity<String> save(Role role);
    public ResponseEntity<String> update(Role role);
    public ResponseEntity<String> deleteById(Integer id);

}
