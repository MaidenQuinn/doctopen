package co.simplon.promo16.doctopen.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.doctopen.dao.AppointmentDAO;
import co.simplon.promo16.doctopen.model.Appointment;

@Repository
public class AppointmentDAOImpl implements AppointmentDAO {
    private static final String CREATE_APPOINTMENT = "INSERT INTO appointment(date) VALUES (?);";
    private static final String GET_APPOINTMENTS = "SELECT * FROM appointment;";
    private static final String GET_APPOINTMENT_BY_ID = "SELECT * FROM appointment WHERE id_appointment = ?;";
    private static final String UPDATE_APPOINTMENT_BY_ID = "UPDATE appointment SET date=? WHERE id_appointment=?;";
    private static final String DELETE_APPOINTMENT_BY_ID = "DELETE FROM appointment WHERE id_appointment=?;";

    @Autowired
    private DataSource dataSource;

    private Connection connection;

    @Override
    public List<Appointment> findAll() {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(GET_APPOINTMENTS);
            ResultSet result = stmt.executeQuery();
            List<Appointment> appointmentList = new ArrayList<>();
            while (result.next()) {
                Appointment appointment = new Appointment(
                        result.getInt("id"),
                        result.getDate("date").toLocalDate());
                appointmentList.add(appointment);
            }
            connection.close();
            return appointmentList;
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Appointment findById(Integer id) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(GET_APPOINTMENT_BY_ID);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                Appointment appointment = new Appointment(
                        result.getInt("id"),
                        result.getDate("date").toLocalDate());
                connection.close();
                return appointment;
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ResponseEntity<String> save(Appointment appointment) {
        try {
            connection = dataSource.getConnection();
            if (appointment.getId_appointment() != null) {
                update(appointment);
            }
            PreparedStatement stmt = connection
                    .prepareStatement(CREATE_APPOINTMENT);
            stmt.setDate(1, Date.valueOf(appointment.getDate()));

            int execute = stmt.executeUpdate();
            if (execute == 1) {
                connection.close();
                return new ResponseEntity<>("Appointment successfully created.", HttpStatus.OK);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Error -> Appointment can't be created.", HttpStatus.BAD_REQUEST);

    }

    @Override
    public ResponseEntity<String> update(Appointment appointment) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement(UPDATE_APPOINTMENT_BY_ID);
            stmt.setDate(1, Date.valueOf(appointment.getDate()));
            stmt.setInt(5, appointment.getId_appointment());

            int execute = stmt.executeUpdate();
            if (execute == 1) {
                connection.close();
                return new ResponseEntity<>("Appointment successfully updated.", HttpStatus.OK);
            }

        } catch (SQLException e) {

            e.printStackTrace();
        }
        return new ResponseEntity<>("Error -> Appointment can't be updated.", HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<String> deleteById(Integer id) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(DELETE_APPOINTMENT_BY_ID);
            stmt.setInt(1, id);

            int execute = stmt.executeUpdate();
            if (execute == 1) {
                connection.close();
                return new ResponseEntity<>("Appointment successfully deleted.", HttpStatus.OK);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new ResponseEntity<>("Error -> Appointment can't be deleted.", HttpStatus.BAD_REQUEST);
    }
}
