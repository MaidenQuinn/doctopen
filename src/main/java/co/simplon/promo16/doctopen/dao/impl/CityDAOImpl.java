package co.simplon.promo16.doctopen.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.doctopen.dao.CityDAO;
import co.simplon.promo16.doctopen.model.City;

@Repository
public class CityDAOImpl implements CityDAO {
    private static final String CREATE_CITY = "INSERT INTO city (zipcode,city_name) VALUES (?,?)";
    private static final String GET_CITIES = "SELECT * FROM city";
    private static final String GET_CITY_BY_ID = "SELECT * FROM city WHERE id_city=?";
    private static final String UPDATE_CITY_BY_ID = "UPDATE city zipcode=?, city_name=? WHERE id=?";
    private static final String DELETE_CITY_BY_ID = "DELETE FROM appointment WHERE id_appointment=?";

    @Autowired
    DataSource dataSource;

    private Connection connection;

    @Override
    public List<City> findAll() {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(GET_CITIES);
            ResultSet result = stmt.executeQuery();
            List<City> cityList = new ArrayList<>();
            while (result.next()) {
                City city = new City(
                        result.getInt("id_city"),
                        result.getInt("zipcode"),
                        result.getString("city_name"));
                cityList.add(city);
            }
            connection.close();
            return cityList;
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;

    }

    @Override
    public City findById(Integer id) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(GET_CITY_BY_ID);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                City city = new City(
                        result.getInt("id_city"),
                        result.getInt("zipcode"),
                        result.getString("city_name"));
                connection.close();
                return city;
            }

        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ResponseEntity<String> save(City city) {
        try {
            connection = dataSource.getConnection();
            if (city.getId_city() != null) {
                return update(city);
            }
            PreparedStatement stmt = connection.prepareStatement(CREATE_CITY);
            stmt.setInt(1, city.getZipcode());
            stmt.setString(2, city.getCity_name());

            int execute = stmt.executeUpdate();
            if (execute == 1) {
                connection.close();
                return new ResponseEntity<>("City successfully created.", HttpStatus.OK);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Error -> City can't be created.", HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<String> update(City city) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(UPDATE_CITY_BY_ID);
            stmt.setInt(1, city.getId_city());
            stmt.setString(2, city.getCity_name());
            stmt.setInt(3, city.getId_city());

            int execute = stmt.executeUpdate();
            if (execute == 1) {
                connection.close();
                return new ResponseEntity<>("City successfully updated.", HttpStatus.OK);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Error -> City can't be updated.", HttpStatus.BAD_REQUEST);

    }

    @Override
    public ResponseEntity<String> deleteById(Integer id) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(DELETE_CITY_BY_ID);
            stmt.setInt(1, id);

            int execute = stmt.executeUpdate();
            if (execute == 1) {
                connection.close();
                return new ResponseEntity<>("City successfully deleted.", HttpStatus.OK);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new ResponseEntity<>("Error -> City can't be deleted.", HttpStatus.BAD_REQUEST);
    }

}
