package co.simplon.promo16.doctopen.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.doctopen.dao.JobDAO;
import co.simplon.promo16.doctopen.model.Job;

@Repository
public class JobDAOImpl implements JobDAO {
    private static final String CREATE_JOB = "INSERT INTO job (job_name,speciality) VALUES (?,?);";
    private static final String GET_JOBS = "SELECT * FROM job;";
    private static final String GET_JOB_BY_ID = "SELECT * FROM job WHERE id_job = ?;";
    private static final String UPDATE_JOB_BY_ID = "UPDATE job job_name=?, speciality=? WHERE id=?;";
    private static final String DELETE_JOB_BY_ID = "DELETE FROM job WHERE id_job=?;";

    @Autowired
    private DataSource dataSource;

    private Connection connection;

    @Override
    public List<Job> findAll() {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(GET_JOBS);
            ResultSet result = stmt.executeQuery();
            List<Job> jobList = new ArrayList<>();
            while (result.next()) {
                Job job = new Job(
                        result.getInt("id_job"),
                        result.getString("job_name"),
                        result.getString("speciality"));
                jobList.add(job);
            }
            connection.close();
            return jobList;
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;

    }

    @Override
    public Job findById(Integer id_job) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(GET_JOB_BY_ID);
            stmt.setInt(1, id_job);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                Job job = new Job(
                        result.getInt("id_job"),
                        result.getString("job_name"),
                        result.getString("speciality"));
                connection.close();
                return job;
            }

        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ResponseEntity<String> save(Job job) {
        try {
            connection = dataSource.getConnection();
            if (job.getId_job() != null) {
                return update(job);
            }
            PreparedStatement stmt = connection.prepareStatement(CREATE_JOB);
            stmt.setString(1, job.getJob_name());
            stmt.setString(2, job.getSpeciality());

            int execute = stmt.executeUpdate();
            if (execute == 1) {
                connection.close();
                return new ResponseEntity<>("Job successfully created.", HttpStatus.OK);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Error -> Job can't be created.", HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<String> update(Job job) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(UPDATE_JOB_BY_ID);
            stmt.setString(1, job.getJob_name());
            stmt.setString(2, job.getSpeciality());
            stmt.setInt(3, job.getId_job());

            int execute = stmt.executeUpdate();
            if (execute == 1) {
                connection.close();
                return new ResponseEntity<>("Job successfully updated.", HttpStatus.OK);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Error -> Job can't be updated.", HttpStatus.BAD_REQUEST);

    }

    @Override
    public ResponseEntity<String> deleteById(Integer id) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(DELETE_JOB_BY_ID);
            stmt.setInt(1, id);

            int execute = stmt.executeUpdate();
            if (execute == 1) {
                connection.close();
                return new ResponseEntity<>("Job successfully deleted.", HttpStatus.OK);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new ResponseEntity<>("Error -> Job can't be deleted.", HttpStatus.BAD_REQUEST);
    }
}
