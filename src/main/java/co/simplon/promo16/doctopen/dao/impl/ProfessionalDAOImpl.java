package co.simplon.promo16.doctopen.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.doctopen.dao.ProfessionalDAO;
import co.simplon.promo16.doctopen.model.Professional;

@Repository
public class ProfessionalDAOImpl implements ProfessionalDAO {
    private static final String CREATE_PROFESSIONAL = "INSERT INTO professional (lastname, firstname, mail, address, phone, activity_description, schedule) VALUES (?, ?, ?, ?, ?, ?, ?);";
    private static final String GET_PROFESSIONALS = "SELECT * FROM professional;";
    private static final String GET_PROFESSIONAL_BY_ID = "SELECT * FROM professional WHERE id=?;";
    private static final String UPDATE_PROFESSIONAL_BY_ID = "UPDATE professional SET lastname = ?, firstname = ?, mail = ?, address = ?, phone = ?, activity_description = ?, schedule = ? WHERE id=?;";
    private static final String DELETE_PROFESSIONAL_BY_ID = "DELETE FROM professional WHERE id=?;";

    @Autowired
    private DataSource dataSource;

    private Connection connection;

    @Override
    public List<Professional> findAll() {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(GET_PROFESSIONALS);
            ResultSet result = stmt.executeQuery();
            List<Professional> professionalList = new ArrayList<>();
            while (result.next()) {
                Professional professional = new Professional(
                        result.getInt("id_professinal"),
                        result.getString("lastname"),
                        result.getString("firstname"),
                        result.getString("address"));
                professionalList.add(professional);
            }
            connection.close();
            return professionalList;
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;

    }

    @Override
    public Professional findById(Integer id) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(GET_PROFESSIONAL_BY_ID);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                Professional professional = new Professional(
                        result.getInt("id_professinal"),
                        result.getString("lastname"),
                        result.getString("firstname"),
                        result.getString("mail"),
                        result.getString("address"),
                        result.getInt("phone"),
                        result.getString("activity_description"),
                        result.getString("schedule"));
                connection.close();
                return professional;
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ResponseEntity<String> save(Professional professional) {
        try {
            connection = dataSource.getConnection();
            if (professional.getId_professional() != null) {
                return update(professional);
            }

            PreparedStatement stmt = connection.prepareStatement(CREATE_PROFESSIONAL);
            stmt.setString(1, professional.getLastName());
            stmt.setString(2, professional.getFirstName());
            stmt.setString(3, professional.getMail());
            stmt.setString(4, professional.getAddress());
            stmt.setInt(5, professional.getPhone());
            stmt.setString(6, professional.getDescription());
            stmt.setString(7, professional.getSchedule());

            int execute = stmt.executeUpdate();
            if (execute == 1) {
                connection.close();
                return new ResponseEntity<>("Professional successfully created.", HttpStatus.OK);
            }

        } catch (SQLException e) {

            e.printStackTrace();
        }

        return new ResponseEntity<>("Error -> Appointment can't be created.", HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<String> update(Professional professional) {
        try {
            PreparedStatement stmt = connection.prepareStatement(UPDATE_PROFESSIONAL_BY_ID);
            stmt.setString(1, professional.getLastName());
            stmt.setString(2, professional.getFirstName());
            stmt.setString(3, professional.getMail());
            stmt.setString(4, professional.getAddress());
            stmt.setInt(5, professional.getPhone());
            stmt.setString(6, professional.getDescription());
            stmt.setString(7, professional.getSchedule());
            stmt.setInt(8, professional.getId_professional());
            
            
            int execute = stmt.executeUpdate();
            if (execute == 1) {
                connection.close();
                return new ResponseEntity<>("Professional successfully updated.", HttpStatus.OK);
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return new ResponseEntity<>("Error -> Professional can't be updated.", HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<String> deleteById(Integer id) {
        try {
            PreparedStatement stmt = connection.prepareStatement(DELETE_PROFESSIONAL_BY_ID);
            stmt.setInt(1, id);
            
            
            int execute = stmt.executeUpdate();
            if (execute == 1) {
                connection.close();
                return new ResponseEntity<>("Professional successfully deleted.", HttpStatus.OK);
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return new ResponseEntity<>("Error -> Professional can't be deleted.", HttpStatus.BAD_REQUEST);
    }
}
