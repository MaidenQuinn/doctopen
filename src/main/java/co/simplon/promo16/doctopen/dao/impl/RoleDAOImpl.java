package co.simplon.promo16.doctopen.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.doctopen.dao.RoleDAO;
import co.simplon.promo16.doctopen.model.Role;

@Repository
public class RoleDAOImpl implements RoleDAO {
    private static final String CREATE_ROLE = "INSERT INTO role(role_name) VALUES (?);";
    private static final String GET_ROLES = "SELECT * FROM role;";
    private static final String GET_ROLE_BY_ID = "SELECT * FROM role WHERE id=?;";
    private static final String UPDATE_ROLE_BY_ID = "UPDATE role SET role_name=? WHERE id=?;";
    private static final String DELETE_ROLE_BY_ID = "DELETE FROM role WHERE id=?;";

    @Autowired
    private DataSource dataSource;

    private Connection connection;

    @Override
    public List<Role> findAll() {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(GET_ROLES);
            ResultSet result = stmt.executeQuery();
            List<Role> roleList = new ArrayList<>();
            while (result.next()) {
                Role role = new Role(
                        result.getInt("id"),
                        result.getString("role_name"));
                roleList.add(role);
            }
            connection.close();
            return roleList;
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Role findById(Integer id) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(GET_ROLE_BY_ID);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                Role role = new Role(
                        result.getInt("id"),
                        result.getString("role_name"));

                connection.close();
                return role;
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ResponseEntity<String> save(Role role) {
        try {
            connection = dataSource.getConnection();
            if (role.getId_role() != null) {
                update(role);
            }
            PreparedStatement stmt = connection.prepareStatement(CREATE_ROLE);
            stmt.setString(1, role.getRole_name());

            int execute = stmt.executeUpdate();
            if (execute == 1) {
                connection.close();
                return new ResponseEntity<>("Role successfully created.", HttpStatus.OK);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Error -> Role can't be created.", HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<String> update(Role role) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(UPDATE_ROLE_BY_ID);
            stmt.setString(1, role.getRole_name());
            stmt.setInt(3, role.getId_role());

            int execute = stmt.executeUpdate();
            if (execute == 1) {
                connection.close();
                return new ResponseEntity<>("Role successfully updated.", HttpStatus.OK);
            }

        } catch (SQLException e) {

            e.printStackTrace();
        }
        return new ResponseEntity<>("Error -> Role can't be updated.", HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<String> deleteById(Integer id) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(DELETE_ROLE_BY_ID);
            stmt.setInt(1, id);

            int execute = stmt.executeUpdate();
            if (execute == 1) {
                connection.close();
                return new ResponseEntity<>("Role successfully deleted.", HttpStatus.OK);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Error -> Role can't be deleted.", HttpStatus.BAD_REQUEST);

    }
}
