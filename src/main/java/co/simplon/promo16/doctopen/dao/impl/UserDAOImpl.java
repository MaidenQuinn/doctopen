package co.simplon.promo16.doctopen.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.doctopen.dao.UserDAO;
import co.simplon.promo16.doctopen.model.Role;
import co.simplon.promo16.doctopen.model.User;

@Repository
public class UserDAOImpl implements UserDAO {
    private static final String CREATE_USER = "INSERT INTO user(username, password) VALUES (?,?);";
    private static final String GET_USERS = "SELECT * FROM user;";
    private static final String GET_USER_BY_ID = "SELECT * FROM user;";
    private static final String UPDATE_USER_BY_ID = "UPDATE user SET username=?, password=? WHERE id_user=?;";
    private static final String DELETE_USER_BY_ID = "DELETE FROM user WHERE id_user=?;";

    @Autowired
    private DataSource dataSource;
    
    private Connection connection;
    private CustomerDAOImpl customerRepository;

    @Override
    public ResponseEntity<String> save(User user, Role role) {
        try {
            connection = dataSource.getConnection();
            if (user.getId_user() != null) {
                update(user);
            }
            PreparedStatement stmt = connection.prepareStatement(CREATE_USER);
            stmt.setString(1, user.getUsername());
            stmt.setString(2, user.getPassword());
            
            int execute = stmt.executeUpdate();

            PreparedStatement stmtBis = connection.prepareStatement("INSERT INTO user_role(user_id, role_id) VALUES (?,?);");
            stmtBis.setInt(1, 1);

            int executeBis = stmtBis.executeUpdate();
            if (execute == 1 && executeBis == 1) {
                connection.close();
                return new ResponseEntity<>("User successfully created.", HttpStatus.OK);
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Error -> User can't be created.", HttpStatus.BAD_REQUEST);

    }

    @Override
    public List<User> findAll() {
        try {
            PreparedStatement stmt = connection.prepareStatement(GET_USER_BY_ID);
            ResultSet result = stmt.executeQuery();
            List<User> userList = new ArrayList<>();
            while (result.next()) {
                User user = new User(
                        result.getInt("id_user"),
                        result.getString("username"),
                        result.getString("password"));
                userList.add(user);
            }
            return userList;
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public User findById(Integer id) {
        try {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM user WHERE id_user=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new User(
                        result.getInt("id_user"),
                        result.getString("username"));
            }
            return null;
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ResponseEntity<String> update(User user) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement(UPDATE_USER_BY_ID);
            stmt.setString(1, user.getUsername());
            stmt.setString(2, user.getPassword());
            stmt.setInt(3, user.getId_user());

            int execute = stmt.executeUpdate();
            if (execute == 1) {
                connection.close();
                return new ResponseEntity<>("User successfully updated.", HttpStatus.OK);
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return new ResponseEntity<>("Error -> User can't be updated.", HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<String> deleteById(Integer id) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(DELETE_USER_BY_ID);
            stmt.setInt(1, id);

            int execute = stmt.executeUpdate();
            if (execute == 1) {
                connection.close();
                return new ResponseEntity<>("User successfully deleted.", HttpStatus.OK);
            }            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Error -> User can't be deleted.", HttpStatus.BAD_REQUEST);

    }

    public User findByIdWithCustomers(Integer id) {
        PreparedStatement stmt;
        try {
            stmt = connection
                    .prepareStatement("SELECT * FROM user WHERE id_user=?");

            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                User user = new User(
                        result.getInt("id_user"),
                        result.getString("name"));
                // user.setCustomers(customerRepository.findById(id));
                return user;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
