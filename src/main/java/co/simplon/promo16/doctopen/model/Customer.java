package co.simplon.promo16.doctopen.model;

import java.time.LocalDate;

public class Customer {

    private Integer id_customer;
    private String lastName;
    private String firstName;
    private LocalDate birthday;
    private String mail;

    public Customer(Integer id_customer, String lastName, String firstName, LocalDate birthday, String mail) {
        this.id_customer = id_customer;
        this.lastName = lastName;
        this.firstName = firstName;
        this.birthday = birthday;
        this.mail = mail;
    }

    public Customer(String lastName, String firstName, LocalDate birthday, String mail) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.birthday = birthday;
        this.mail = mail;
    }

    public Customer() {
    }

    public Integer getId_customer() {
        return id_customer;
    }

    public void setId_customer(Integer id_customer) {
        this.id_customer = id_customer;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

}
