package co.simplon.promo16.doctopen.model;

public class Job {
    private Integer id_job;
    private String job_name;
    private String speciality;

    public Job(String job_name, String speciality) {
        this.job_name = job_name;
        this.speciality = speciality;
    }

    public Job(Integer id_job, String job_name, String speciality) {
        this.id_job = id_job;
        this.job_name = job_name;
        this.speciality = speciality;
    }

    public Job() {
    }

    public Integer getId_job() {
        return id_job;
    }


    public void setId_job(Integer id_job) {
        this.id_job = id_job;
    }

    public String getJob_name() {
        return job_name;
    }

    public void setJob_name(String job_name) {
        this.job_name = job_name;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }
}
