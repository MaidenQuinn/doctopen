package co.simplon.promo16.doctopen.model;

public class Role {
    private Integer id_role;
    private String role_name;

    public Role() {
    }

    public Role(String role_name) {
        this.role_name = role_name;
    }

    public Role(Integer id_role, String role_name) {
        this.id_role = id_role;
        this.role_name = role_name;
    }

    public Integer getId_role() {
        return id_role;
    }

    public void setId_role(Integer id_role) {
        this.id_role = id_role;
    }

    public String getRole_name() {
        return role_name;
    }

    public void setRole_name(String role_name) {
        this.role_name = role_name;
    }
}
